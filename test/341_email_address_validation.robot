# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}    Get Variable Value    ${TEST SETUP}
	${__TEST_341_SETUP}    Get Variable Value    ${TEST 341 SETUP}
	Run Keyword If    $__TEST_SETUP is not None    ${__TEST_SETUP}
	Run Keyword If    $__TEST_341_SETUP is not None    ${__TEST_341_SETUP}

Test Teardown
	${__TEST_TEARDOWN}    Get Variable Value    ${TEST TEARDOWN}
	${__TEST_341_TEARDOWN}    Get Variable Value    ${TEST 341 TEARDOWN}
	Run Keyword If    $__TEST_341_TEARDOWN is not None    ${__TEST_341_TEARDOWN}
	Run Keyword If    $__TEST_TEARDOWN is not None    ${__TEST_TEARDOWN}

*** Test Cases ***
email address validation
    [Setup]    Test Setup

	${email_address} =	Get Test Param	DS_email_address
	${error_message} =	Get Test Param	DS_error_message

	Given user is on login page
	When User creates account with ${email_address}
	Then ${error_message} is displayed

    [Teardown]    Test Teardown
