*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}    Get Variable Value    ${TEST SETUP}
	${__TEST_341_SETUP}    Get Variable Value    ${TEST 341 SETUP}
	Run Keyword If    $__TEST_SETUP is not None    ${__TEST_SETUP}
	Run Keyword If    $__TEST_341_SETUP is not None    ${__TEST_341_SETUP}

Test Teardown
	${__TEST_TEARDOWN}    Get Variable Value    ${TEST TEARDOWN}
	${__TEST_341_TEARDOWN}    Get Variable Value    ${TEST 341 TEARDOWN}
	Run Keyword If    $__TEST_341_TEARDOWN is not None    ${__TEST_341_TEARDOWN}
	Run Keyword If    $__TEST_TEARDOWN is not None    ${__TEST_TEARDOWN}

*** Test Cases ***
email address validation
    [Setup]    Test Setup
	Given user is on login page
	When User creates account with alice@mail
	Then Invalid email address. is displayed
	[Teardown]    Test Teardown
